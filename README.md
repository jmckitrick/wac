# WAC - Write A Compiler

## Simple C compiler in Clojure (Nora Sandler - https://norasandler.com/2017/11/29/Write-a-Compiler.html)

## Uses tools.deps (not leiningen)

## Requirements:

- gcc-9

## Running:

`> clj -m core c/stage-1/valid/return_2.c`

## Compiling:

`> /usr/local/bin/gcc-9 -S -O3 -fno-asynchronous-unwind-tables c/stage-1/valid/return_2.c`

OR

`> /usr/local/bin/gcc-9 -m32 c/stage-1/valid/return_2.c`

## Testing:

`bin/test-compiler.sh ./my-compiler.sh n`

where `n` is the stage number to test.

OR

`> clj -A:test`

OR

`test/test_all.sh`

## Helper script:

`my-compiler.sh`

## Development:

`core> `

## TODO:
