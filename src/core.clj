(ns core
  (:require [clojure.string :as str]))

(def debug-x false)
(def debug-lex false)
(def debug-parse false)
(def debug-codegen false)

(def symbol-table (atom {}))
(def stack-index (atom -8))

;; ==================================================================================
;; ===================================== lexer ======================================
;; ==================================================================================

(def lexer-tokens
  [[:open-brace "\\{"]
   [:close-brace "\\}"]
   [:open-paren "\\("]
   [:close-paren "\\)"]
   [:semicolon ";"]
   [:kw-int "int"]
   [:kw-return "return"]
   [:identifier "[a-zA-Z]\\w*"]
   [:literal-int "[0-9]+"]
   [:negation "-"]
   [:bitwise-complement "~"]
   [:not-equal "!="]
   [:logical-negation "!"]
   [:addition "\\+"]
   [:multiplication "\\*"]
   [:division "\\/"]
   [:and "&&"]
   [:or "\\|\\|"]
   [:equal "=="]
   [:less-than-or-equal "<="]
   [:less-than "<"]
   [:greater-than-or-equal ">="]
   [:greater-than ">"]
   [:assignment "="]])

(defn check-text-for-token [line re]
  (when debug-lex (prn "Check line begin:" line re))
  (let [m (re-find (re-pattern (str "^" re)) line)]
    (when debug-lex (prn "Check line no ws:" line re m))
    m))

(defn strip-starting-ws [line]
  (str/replace-first line #"^\s+" ""))

(defn find-next-token
  "Strip starting whitespace then find a matching token."
  [ws-text]
  (when debug-lex (prn "Lex whitespace and text:" ws-text))
  #_(Thread/sleep 2000)
  (let [text (strip-starting-ws ws-text)
        token-lexeme-pairs (for [[token re] lexer-tokens
                                 :let [lexeme (check-text-for-token text re)]
                                 :when lexeme]
                             [token lexeme])]
    #_(prn "Lex a text result:" result)
    (first token-lexeme-pairs)))

(defn lex-text
  "Lexer returns text as pairs of token and lexemes."
  [text]
  (when debug-lex (prn "Lex text:" text))
  (loop [token-lexeme-pair (find-next-token text)
         token-lexeme-pairs []
         text text]
    #_(prn "Top of loop" token-lexeme-pair token-lexeme-pairs text)
    (if token-lexeme-pair
      (let [[token lexeme] token-lexeme-pair
            remaining-text (subs (strip-starting-ws text) (count lexeme))]
        (when debug-lex
          (prn "Start with text" text)
          (prn "Skip chars" lexeme (count lexeme))
          (prn "Rest of text" remaining-text))
        (recur (find-next-token remaining-text)
               (conj token-lexeme-pairs token-lexeme-pair)
               remaining-text))
      token-lexeme-pairs)))

(defn lex [filename]
  (when debug-lex (prn "Lex:" filename))
  (let [source (slurp filename)]
    (lex-text source)))

;; =====================================================================================
;; ======================================== AST ========================================
;; =====================================================================================

(defprotocol ASTNode
  "One node in the Abstract Syntax Tree."
  (emit [a] "Emit assembly code.")
  (pprint [a] "Pretty-print this node.")
  #_(get-node-type [a]))

(defrecord Program [function]
  ASTNode
  (emit [this]
    (when debug-codegen (prn "Program:" this))
    (println "    # begin program")
    (println "    .globl _main")
    (emit (:function this)))
  (pprint [this]
    (pprint (:function this))))

(defrecord Function [fname statements]
  ASTNode
  (emit [this]
    (when debug-codegen (pr this (:fname this)))
    (println "    # begin function" fname)
    (println (str "_" fname #_(:fname this) ":"))
    ;; stack frame
    (println "    # begin stack frame")
    (println "    pushq   %rbp")
    (println "    movq    %rsp, %rbp")
    (doseq [statement statements]
      (emit statement))
    (println "    # end stack frame")
    (println "    movq    %rbp, %rsp")
    (println "    popq    %rbp")
    (println "    # default return")
    (println "    movq    $0, %rax")
    (println "    ret"))
  (pprint [this]
    (println (str "FUN " (:fname this) ":"))
    (doseq [statement (:statements this)]
      (pprint statement))))

;; ---------------------------------------- Statements ----------------------------------------

(defrecord Return [exp]
  ASTNode
  (emit [this]
    (println "    # return ")
    (emit exp)
    ;; stack frame
    (println "    # end stack frame")
    (println "    movq    %rbp, %rsp")
    (println "    popq    %rbp")
    (println "    ret"))
  (pprint [this]
    (print "    RETURN ")
    (pprint (:exp this))
    (println)))

(defrecord Declare [name exp]
  ASTNode
  (emit [this]
    (when (get @symbol-table name)
      (throw (Exception. (str "Cannot declare" name "again."))))
    (println "    # declare" name)
    (swap! symbol-table assoc name @stack-index)
    (when (:exp this)
      (emit (:exp this))
      (println "    push    %rax")
      (swap! stack-index #(- % 8))))
  (pprint [this]
    (print (str "    INT " (:name this)))
    (when (:exp this)
      (print " = ")
      (pprint (:exp this)))
    (println)))

(defrecord Exp [exp]
  ASTNode
  (emit [this]
    (emit exp)
    )
  (pprint [this]
    ;(print "    EXP? ")
    (print "    ")
    (pprint (:exp this))
    (println)))

;; ---------------------------------------- Expressions ----------------------------------------

(defrecord Constant [val]
  ASTNode
  (emit [this]
    (println (str "    movq    $" (:val this) ", %rax")))
  (pprint [this]
    (print (:val this))))

(defrecord UnOp [op exp]
  ASTNode
  (emit [this]
    (emit (:exp this))
    (case (:op this)
      :negation (println "    neg %eax")
      :bitwise-complement (println "    not %eax")
      :logical-negation
      (do
        (println "    cmpl $0, %eax")
        (println "    movl $0, %eax")
        (println "    sete %al"))))
  (pprint [this]
    (print "(")
    (print (case (:op this)
             :negation "-"
             :bitwise-complement "~"
             :logical-negation "!"))
    ;;(print " ")
    (pprint (:exp this))
    (print ")")))

(def label-counter (atom 0))

(defn gen-label []
  (swap! label-counter inc)
  (str "_label" @label-counter))

(defrecord BinOp [op exp1 exp2]
  ASTNode
  (emit [this]
    ;; (emit (:exp1 this))
    ;; (println "    push    %rax")
    ;; (emit (:exp2 this))
    ;; (println "    pop     %rcx")
    ;; (println "    xchg    %rcx, %rax")
    (case (:op this)
      :addition       (do
                        (emit (:exp1 this))
                        (println "    push    %rax")
                        (emit (:exp2 this))
                        (println "    pop     %rcx")
                        (println "    xchg    %rcx, %rax")
                        (println "    addl    %ecx, %eax"))
      :multiplication (do
                        (emit (:exp1 this))
                        (println "    push    %rax")
                        (emit (:exp2 this))
                        (println "    pop     %rcx")
                        (println "    xchg    %rcx, %rax")
                        (println "    imul    %ecx, %eax"))
      :negation       (do
                        (emit (:exp1 this))
                        (println "    push    %rax")
                        (emit (:exp2 this))
                        (println "    pop     %rcx")
                        (println "    xchg    %rcx, %rax")
                        (println "    subl    %ecx, %eax"))
      :division       (do
                        (emit (:exp1 this))
                        (println "    push    %rax")
                        (emit (:exp2 this))
                        (println "    pop     %rcx")
                        (println "    xchg    %rcx, %rax")
                        (println "    cdq")
                        (println "    idivl   %ecx"))
      :equal          (do
                        (emit (:exp1 this))
                        (println "    push    %rax")
                        (emit (:exp2 this))
                        (println "    pop     %rcx")
                        (println "    xchg    %rcx, %rax")
                        (println "    cmpl %ecx, %eax") ;swap these?
                        (println "    movl $0, %eax")
                        (println "    sete %al"))
      :not-equal      (do
                        (emit (:exp1 this))
                        (println "    push    %rax")
                        (emit (:exp2 this))
                        (println "    pop     %rcx")
                        (println "    xchg    %rcx, %rax")
                        (println "    cmpl %ecx, %eax") ;swap these?
                        (println "    movl $0, %eax")
                        (println "    setne %al"))
      :less-than      (do
                        (emit (:exp1 this))
                        (println "    push    %rax")
                        (emit (:exp2 this))
                        (println "    pop     %rcx")
                        (println "    xchg    %rcx, %rax")
                        (println "    cmpl %ecx, %eax") ;swap these?
                        (println "    movl $0, %eax")
                        (println "    setl %al"))
      :less-than-or-equal (do
                            (emit (:exp1 this))
                            (println "    push    %rax")
                            (emit (:exp2 this))
                            (println "    pop     %rcx")
                            (println "    xchg    %rcx, %rax")
                            (println "    cmpl %ecx, %eax") ;swap these?
                            (println "    movl $0, %eax")
                            (println "    setle %al"))
      :greater-than      (do
                           (emit (:exp1 this))
                           (println "    push    %rax")
                           (emit (:exp2 this))
                           (println "    pop     %rcx")
                           (println "    xchg    %rcx, %rax")
                           (println "    cmpl %ecx, %eax") ;swap these?
                           (println "    movl $0, %eax")
                           (println "    setg %al"))
      :greater-than-or-equal (do
                               (emit (:exp1 this))
                               (println "    push    %rax")
                               (emit (:exp2 this))
                               (println "    pop     %rcx")
                               (println "    xchg    %rcx, %rax")
                               (println "    cmpl %ecx, %eax") ;swap these?
                               (println "    movl $0, %eax")
                               (println "    setge %al"))
      :or (do
            (let [label-1 (gen-label)
                  label-2 (gen-label)]
              (emit (:exp1 this))
              (println "    cmpl    $0, %eax")
              (println "    je     " label-1)
              (println "    movl    $1, %eax")
              (println "    jmp    " label-2)
              (println (str label-1 ":"))
              (emit (:exp2 this))
              (println "    cmpl    $0, %eax")
              (println "    movl    $0, %eax")
              (println "    setne   %al")
              (println (str label-2 ":"))))
      :and  (do
              (let [label-1 (gen-label)
                    label-2 (gen-label)]
                (emit (:exp1 this))
                (println "    cmpl    $0, %eax")
                (println "    jne    " label-1)
                (println "    jmp    " label-2)
                (println (str label-1 ":"))
                (emit (:exp2 this))
                (println "    cmpl    $0, %eax")
                (println "    movl    $0, %eax")
                (println "    setne   %al")
                (println (str label-2 ":"))))))
  (pprint [this]
    (print "(")
    (pprint (:exp1 this))
    (print " ")
    (print (case (:op this)
             :addition "+"
             :negation "-"
             :multiplication "*"
             :division "/"
             :equal "=="
             :not-equal "!="
             :less-than "<"
             :less-than-or-equal "<="
             :greater-than ">"
             :greater-than-or-equal ">="
             :or "||"
             :and "&&"))
    (print " ")
    (pprint (:exp2 this))
    (print ")")))

(defrecord Var [name]
  ASTNode
  (emit [this]
    (println "    # var" name)
    (if-let [index (get @symbol-table name)]
      (println (str "    movq    " index "(%rbp), %rax"))
      (throw (Exception. "Undeclared variable" name))))
  (pprint [this]
    ;(prn "VAR?")
    ;(print (:val this))
    (print (:name this))))

(defrecord Assign [name exp]
  ASTNode
  (emit [this]
    (println "    # assign" name)
    (emit (:exp this))
    (if-let [index (get @symbol-table name)]
      (println (str "    movq    %rax, " index "(%rbp)"))
      (throw (Exception. "Undeclared variable" name))))
  (pprint [this]
    #_(prn "ASSIGN?")
    (print (:name this) "= ")
    (pprint (:exp this))))

(defn un-op? [op]
  (contains? #{:negation :bitwise-complement :logical-negation} op))

;; ========================================================================================
;; ======================================== parser ========================================
;; ========================================================================================

(def parse-expression)

(defn parse-factor
  "Parse parenthetical expressions, unary op/factor, and int."
  [token-lexeme-pairs]
  (let [[token lexeme] (first token-lexeme-pairs)
        token-lexeme-pairs (rest token-lexeme-pairs)]
    (cond
      (= token :open-paren) (let [[exp remaining-token-lexeme-pairs] (parse-expression token-lexeme-pairs)
                                  [token lexeme] (first remaining-token-lexeme-pairs)]
                              (when-not (= token :close-paren)
                                (throw (Exception. (str "Expected close paren, got" token))))
                              [exp (rest remaining-token-lexeme-pairs)])
      (un-op? token) (let [op token
                           [factor remaining-token-lexeme-pairs] (parse-factor token-lexeme-pairs)]
                       [(->UnOp op factor) remaining-token-lexeme-pairs])
      (= token :literal-int) [(->Constant (read-string lexeme)) token-lexeme-pairs]
      (= token :identifier) [(->Var lexeme) token-lexeme-pairs]
      :else (throw (Exception. "Factor is not expression, unary op, or int.")))))

(defn parse-term
  "Parse factors and * / binary operators."
  [token-lexeme-pairs]
  (let [[factor remaining-token-lexeme-pairs] (parse-factor token-lexeme-pairs)]
    (when debug-parse (prn "term > factor" factor))
    (loop [remaining-token-lexeme-pairs remaining-token-lexeme-pairs
           factor factor]
      (let [[next-token _] (first remaining-token-lexeme-pairs)]
        (when debug-parse (prn "term > factor > loop next-token" next-token))
        (if (or (= next-token :multiplication) (= next-token :division))
          (let [op next-token
                [next-factor remaining-token-lexeme-pairs] (parse-factor (rest remaining-token-lexeme-pairs))
                factor (->BinOp op factor next-factor)]
            (when debug-parse (prn "term > factor > loop recur op" op "next-f" next-factor "f" factor))
            (recur remaining-token-lexeme-pairs factor))
          [factor remaining-token-lexeme-pairs])))))

(defn parse-additive-expression
  "Parse +/- expression."
  [token-lexeme-pairs]
  (let [[term remaining-token-lexeme-pairs] (parse-term token-lexeme-pairs)]
    (when debug-parse (prn "expression > term" term))
    (loop [remaining-token-lexeme-pairs remaining-token-lexeme-pairs
           term term]
      (let [[next-token _] (first remaining-token-lexeme-pairs)]
        (when debug-parse (prn "expression > term > loop next-token" next-token))
        (if (or (= next-token :addition) (= next-token :negation))
          (let [op next-token
                [next-term remaining-token-lexeme-pairs] (parse-term (rest remaining-token-lexeme-pairs))
                term (->BinOp op term next-term)]
            (when debug-parse (prn "term > factor > loop recur op" op "next-f" next-term "f" term))
            (recur remaining-token-lexeme-pairs term))
          [term remaining-token-lexeme-pairs])))))

(defn parse-relational-expression
  [token-lexeme-pairs]
  (let [[exp remaining-token-lexeme-pairs] (parse-additive-expression token-lexeme-pairs)]
    #_(when debug-parse (prn "expression > term" term))
    (loop [remaining-token-lexeme-pairs remaining-token-lexeme-pairs
           exp exp]
      (let [[next-token _] (first remaining-token-lexeme-pairs)]
        #_(when debug-parse (prn "expression > term > loop next-token" next-token))
        (if (or (= next-token :less-than-or-equal)
                (= next-token :less-than)
                (= next-token :greater-than-or-equal)
                (= next-token :greater-than))
          (let [op next-token
                [next-exp remaining-token-lexeme-pairs] (parse-additive-expression (rest remaining-token-lexeme-pairs))
                exp (->BinOp op exp next-exp)]
            #_(when debug-parse (prn "term > factor > loop recur op" op "next-f" next-term "f" term))
            (recur remaining-token-lexeme-pairs exp))
          [exp remaining-token-lexeme-pairs])))))

(defn parse-equality-expression
  [token-lexeme-pairs]
  (let [[exp remaining-token-lexeme-pairs] (parse-relational-expression token-lexeme-pairs)]
    #_(when debug-parse (prn "expression > term" term))
    (loop [remaining-token-lexeme-pairs remaining-token-lexeme-pairs
           exp exp]
      (let [[next-token _] (first remaining-token-lexeme-pairs)]
        #_(when debug-parse (prn "expression > term > loop next-token" next-token))
        (if (or (= next-token :equal)
                (= next-token :not-equal))
          (let [op next-token
                [next-exp remaining-token-lexeme-pairs] (parse-relational-expression (rest remaining-token-lexeme-pairs))
                exp (->BinOp op exp next-exp)]
            #_(when debug-parse (prn "term > factor > loop recur op" op "next-f" next-term "f" term))
            (recur remaining-token-lexeme-pairs exp))
          [exp remaining-token-lexeme-pairs])))))

(defn parse-logical-and-expression
  [token-lexeme-pairs]
  (let [[exp remaining-token-lexeme-pairs] (parse-equality-expression token-lexeme-pairs)]
    #_(when debug-parse (prn "expression > term" term))
    (loop [remaining-token-lexeme-pairs remaining-token-lexeme-pairs
           exp exp]
      (let [[next-token _] (first remaining-token-lexeme-pairs)]
        #_(when debug-parse (prn "expression > term > loop next-token" next-token))
        (if (= next-token :and)
          (let [op next-token
                [next-exp remaining-token-lexeme-pairs] (parse-equality-expression (rest remaining-token-lexeme-pairs))
                exp (->BinOp op exp next-exp)]
            #_(when debug-parse (prn "term > factor > loop recur op" op "next-f" next-term "f" term))
            (recur remaining-token-lexeme-pairs exp))
          [exp remaining-token-lexeme-pairs])))))

(defn parse-logical-or-expression
  "Parse terms and binary operators."
  [token-lexeme-pairs]
  #_(prn "parse or")
  (let [[exp remaining-token-lexeme-pairs] (parse-logical-and-expression token-lexeme-pairs)]
    #_(when debug-parse (prn "expression > term" term))
    (loop [remaining-token-lexeme-pairs remaining-token-lexeme-pairs
           exp exp]
      (let [[next-token _] (first remaining-token-lexeme-pairs)]
        #_(when debug-parse (prn "expression > term > loop next-token" next-token))
        (if (= next-token :or)
          (let [op next-token
                [next-exp remaining-token-lexeme-pairs] (parse-logical-and-expression (rest remaining-token-lexeme-pairs))
                exp (->BinOp op exp next-exp)]
            #_(when debug-parse (prn "term > factor > loop recur op" op "next-f" next-term "f" term))
            (recur remaining-token-lexeme-pairs exp))
          [exp remaining-token-lexeme-pairs])))))

(defn parse-assignment
  [token-lexeme-pairs]
  (let [[token name] (first token-lexeme-pairs)
        mid-tl-pairs (rest token-lexeme-pairs)]
    #_(prn "parse-assignment token" token)
    (if (= token :identifier)
      (do
        (let [[token _] (first mid-tl-pairs)
              remaining-tl-pairs (rest mid-tl-pairs)]
          #_(prn "parse-assignment operator token" token)
          (when (not (= token :assignment))
            (throw (Exception. (str "Assignment expression missing '='" token))))
          #_(prn "parse-assignment name" name)
          (let [[exp last-tl-pairs] (parse-expression remaining-tl-pairs)]
            #_(prn "parse-assignment assign name/exp" name exp)
            [(->Assign name exp) last-tl-pairs])))
      ;; not reached
      (throw (Exception. (str "Assignment expression missing identifier."))))))

(defn parse-initialize
  [token-lexeme-pairs]
  (let [[token name] (first token-lexeme-pairs)
        mid-tl-pairs (rest token-lexeme-pairs)]
    #_(prn "parse-initialize token" token)
    (when (not (= token :assignment))
        (throw (Exception. (str "Assignment expression missing '=' " token))))
    (let [[exp remaining-tl-pairs] (parse-expression mid-tl-pairs)]
      #_(prn "parse-initialize token/val" token exp)
      #_(prn "parse-initialize remaining" remaining-tl-pairs)
      [exp remaining-tl-pairs])))

(defn parse-expression
  [token-lexeme-pairs]
  (let [[token name] (first token-lexeme-pairs)]
    #_(prn "parse-expression" token name (first (rest token-lexeme-pairs)))
    (if (and (= :identifier token)
             (= :assignment (first (first (rest token-lexeme-pairs)))))
      (parse-assignment token-lexeme-pairs)
      (parse-logical-or-expression token-lexeme-pairs))))

(defn parse-statement-end
  "Parse `;`"
  [token-lexeme-pairs]
  #_(prn "parse-statement-end" (first token-lexeme-pairs))
  (let [[token _] (first token-lexeme-pairs)]
    (when debug-parse (prn token token-lexeme-pairs))
    (= token :semicolon)
    #_(when-not (= token :semicolon)
      (throw (Exception. "Expected semicolon.")))))

(defn parse-return-statement-body
  "Parse return expression and semicolon."
  [token-lexeme-pairs]
  (let [[token _] (first token-lexeme-pairs)
        remaining-token-lexeme-pairs (rest token-lexeme-pairs)]
    (if (not (= token :kw-return))
      (throw (Exception. "This should not happen.")))
    (let [[exp remaining-token-lexeme-pairs] (parse-expression remaining-token-lexeme-pairs)]
      (parse-statement-end remaining-token-lexeme-pairs)
      [(->Return exp) (rest remaining-token-lexeme-pairs)])))

(defn parse-expression-statement-body
  "Parse expression and semicolon."
  [token-lexeme-pairs]
  (let [[exp remaining-token-lexeme-pairs] (parse-expression token-lexeme-pairs)]

    (parse-statement-end remaining-token-lexeme-pairs)
    [(->Exp exp) (rest remaining-token-lexeme-pairs)]))

(defn parse-declare-statement-body
  "Parse declare expression and semicolon."
  [token-lexeme-pairs]
  #_(prn "parse-declare-statement-body")
  (let [[token _] (first token-lexeme-pairs)
        remaining-token-lexeme-pairs (rest token-lexeme-pairs)]
    (if (not (= token :kw-int))
      (throw (Exception. "This should not happen.")))
    (let [[token name] (first remaining-token-lexeme-pairs)
          remaining-token-lexeme-pairs (rest remaining-token-lexeme-pairs)]
      #_(prn "parse-declare-statement-body" token name)
      #_(prn "parse-declare-statement-body done/initialized?" (first remaining-token-lexeme-pairs))
      (if (parse-statement-end remaining-token-lexeme-pairs)
        [(->Declare name nil) (rest remaining-token-lexeme-pairs)]
        (let [[exp remaining-token-lexeme-pairs] (parse-initialize remaining-token-lexeme-pairs)]
          (if (parse-statement-end remaining-token-lexeme-pairs)
            [(->Declare name exp) (rest remaining-token-lexeme-pairs)]
            (throw (Exception. "Expected semicolon."))))))))

(defn parse-statement
  "Parse `return` and statement."
  [token-lexeme-pairs]
  (let [[token _] (first token-lexeme-pairs)
        remaining-token-lexeme-pairs (rest token-lexeme-pairs)]
    #_(prn "parse-statement" token)
    (case token
      :kw-int (parse-declare-statement-body token-lexeme-pairs)
      :kw-return (parse-return-statement-body token-lexeme-pairs)
      (parse-expression-statement-body token-lexeme-pairs))))

(defn parse-function-open
  "Parse `{`"
  [token-lexeme-pairs]
  (let [[token _] (first token-lexeme-pairs)]
    (when debug-parse (prn token token-lexeme-pairs))
    (when-not (= token :open-brace)
      (throw (Exception. "Expected open brace.")))))

(defn parse-function-close
  "Parse `}`"
  [token-lexeme-pairs]
  (let [[token remaining-token-lexeme-pairs] (first token-lexeme-pairs)]
    #_(prn "parse-function-close?" token)
    (when debug-parse (prn token token-lexeme-pairs))
    (= token :close-brace)))

(defn parse-function-body
  "Parse statement to function close."
  [token-lexeme-pairs func-name]
  #_(prn "parse-function-body")
  (loop [token-lexeme-pairs token-lexeme-pairs
         func-body []]
    (if (parse-function-close token-lexeme-pairs)
      [(->Function func-name func-body) (rest token-lexeme-pairs)]
      (let [[statement remaining-token-lexeme-pairs] (parse-statement token-lexeme-pairs)]
        (recur remaining-token-lexeme-pairs (conj func-body statement))))))

(defn parse-function-block
  "Parse `{` and function body."
  [token-lexeme-pairs func-name]
  (let [[token _] (first token-lexeme-pairs)
        token-lexeme-pairs (rest token-lexeme-pairs)]
    (when-not (= token :open-brace)
      (throw (Exception. "Expected open brace.")))
    (parse-function-body token-lexeme-pairs func-name)))

(defn parse-arglist
  "Parse `()` then function block."
  [token-lexeme-pairs func-name]
  (let [[token _] (first token-lexeme-pairs)
        mid-token-lexeme-pairs (rest token-lexeme-pairs)]
    (when-not (= token :open-paren)
      (throw (Exception. "Expected open paren.")))
    (let [[token _] (first mid-token-lexeme-pairs)
          remaining-token-lexeme-pairs (rest mid-token-lexeme-pairs)]
      (when-not (= token :close-paren)
        (throw (Exception. "Expected close paren in arglist.")))
      (parse-function-block remaining-token-lexeme-pairs func-name))))

(defn parse-function-declaration
  "Parse function identifier then arglist."
  [token-lexeme-pairs]
  (let [[token func-name] (first token-lexeme-pairs)
        remaining-token-lexeme-pairs (rest token-lexeme-pairs)]
    (when-not (= token :identifier)
      (throw (Exception. "Expected function identifier.")))
    (parse-arglist remaining-token-lexeme-pairs func-name)))

(defn parse-function
  "Parse return type then function declaration."
  [token-lexeme-pairs]
  (let [[token _] (first token-lexeme-pairs)
        remaining-token-lexeme-pairs (rest token-lexeme-pairs)]
    (when-not (= token :kw-int)
      (throw (Exception. "Expected 'int' before function declaration.")))
    #_(when debug-parse (prn (first token-lexeme-pairs)))
    (parse-function-declaration remaining-token-lexeme-pairs)))

(defn parse
  "Parse token/lexeme pairs and return an AST.
  Assume the program file consists of one function definition."
  [token-lexeme-pairs]
  #_(prn "Parse program" token-lexeme-pairs)
  (let [[function token-lexeme-pairs] (parse-function token-lexeme-pairs)]
    (when-not (empty? token-lexeme-pairs)
      (throw (Exception. "Extra tokens after main function.")))
    (->Program function)))

;; codegen

(defn generate
  "Traverse an ast and generate code."
  [ast]
  (emit ast))

(defn pretty-print [ast]
  (pprint ast))

(defn -main [filename]
  #_(prn "filename" filename)
  (let [base-filename (second (str/split filename #"\."))
        output-filename (str "." base-filename ".s")]
    #_(prn "base" base-filename)
    #_(prn "output" output-filename)
    (with-open [s (clojure.java.io/output-stream output-filename)
                o (clojure.java.io/writer s)]
      (try
        (if debug-x
          (-> filename
              lex
              ;prn

              parse
              ;prn
              ;pretty-print

              generate)
          (binding [*out* o]
            (-> filename
                lex
                parse
                generate)))
        (catch Exception e
          (println "Compilation failed:" (.getMessage e))
          (prn "Compilation failed:" (.getStackTrace e))
          (prn (.getStackTrace e)))))))
