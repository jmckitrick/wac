#!/bin/sh

##################################
# NOTE
##################################

# The source file must begin with dotslash!!!
# Fix this!

# TODO:
# 1. Output assembly to file.
# 2. Output error to stdout.
# 3. Return correct error code.

DEBUG=1

PROG=$1
BASE="./${PROG%.*}"
EXE=${BASE}
#EXE=$(basename ${BASE})

if [ $DEBUG -eq "2" ]; then
    echo prog $PROG
    echo base $BASE
    echo exe  $EXE
fi

rm -rf ${BASE}.s

clojure -M -m core ${PROG} #> ${BASE}.s

if [ -s ${BASE}.s ]; then
    #echo "Compilation succeeded"
    gcc-9 -m32 -w ${BASE}.s -o ${EXE}
    #gcc-9 ${BASE}.s -o ${EXE}
    exit 0
else
    if [ $DEBUG -eq "2" ]; then
        echo "Compilation failed"
        cat ${BASE}.s
    fi

    if [ -f "$BASE.s" ]; then
      rm ${BASE}.s
    else
      # figure out how to check path from prog name
      echo "ASM file not found, not deleting."
    fi
    exit -1
fi

# in source dir
#./${BASE}
#./${BASE}

# in current dir
#./$EXE

#echo $?
