(ns core-test
  (:require [clojure.test :refer :all]
            [core :as c]))

(deftest core-test
  (testing "check-line without whitespace"
    (is (= (c/check-line "int" "int" nil) "int")))
  (testing "check-line with whitespace"
    (is (= (c/check-line "int" " int" nil) nil))))
