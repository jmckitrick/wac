#!/bin/sh

STAGES="1 2 3"

for STAGE in $STAGES; do

    FILES=$(ls c/stage_$STAGE/valid/*)

    for F in $FILES; do
        echo "Stage:" $F
        sh my-compiler.sh $F
    done

done
